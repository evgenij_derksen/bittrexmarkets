﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BittrexMarkets.Hubs.MarketsSubscriptions.MessageArgs
{
    public class MarketDataUpdatedMessageArgs
    {
        public string MarketName { get; set; }
        public string Bid { get; set; }
        public string Ask { get; set; }
        public string Last { get; set; }
    }
}
