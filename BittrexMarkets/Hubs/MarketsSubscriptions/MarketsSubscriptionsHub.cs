﻿using BittrexMarkets.Hubs.MarketsSubscriptions.MessageArgs;
using BittrexMarkets.Services.Data;
using BittrexMarkets.Services.MarketsData;
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BittrexMarkets.Hubs
{
    public class MarketsSubscriptionsHub : Hub
    {
        private readonly IMarketsSubscriptionsService _marketsSubscriptionsService;
        private readonly IDataService _dataService;

        public MarketsSubscriptionsHub(IMarketsSubscriptionsService marketsSubscriptionsService, IDataService dataService)
        {
            _marketsSubscriptionsService = marketsSubscriptionsService;
            _dataService = dataService;
        }

        public async Task AddMarketSubsription(string marketName)
        {
            var result = _marketsSubscriptionsService.AddMarketSubscription(Context.ConnectionId, marketName);

            if (result.IsSuccessful)
            {
                await Groups.AddToGroupAsync(Context.ConnectionId, marketName);
                await Clients.Caller.SendAsync("SubscriptionSuccessful", marketName);
                var marketValues = await _dataService.GetMarketValues(marketName);

                var messageArgs = new MarketDataUpdatedMessageArgs()
                {
                    MarketName = marketValues.MarketName,
                    Ask = marketValues.Ask.ToString(),
                    Bid = marketValues.Bid.ToString(),
                    Last = marketValues.Last.ToString()
                };

                string json = JsonConvert.SerializeObject(messageArgs);
                await Clients.Caller.SendAsync("MarketDataUpdated", json);
            }
            else
            {
                await Clients.Caller.SendAsync("SubscriptionFailed", result.Message);
            }
        }

        public async Task RemoveMarketSubsription(string marketName)
        {
            var isSuccessful = _marketsSubscriptionsService.RemoveMarketSubscription(Context.ConnectionId, marketName);

            if (isSuccessful)
            {
                await Clients.Caller.SendAsync("UnsubscriptionSuccessful", marketName);
            }
        }

        public override async Task OnConnectedAsync()
        {
            await base.OnConnectedAsync();
        }

        public override async Task OnDisconnectedAsync(Exception exception)
        {
            var removedSubscriptions = _marketsSubscriptionsService.RemoveAllSubscriptionsForUser(Context.ConnectionId);

            foreach(var removedSub in removedSubscriptions)
            {
                await Groups.RemoveFromGroupAsync(Context.ConnectionId, removedSub);
            }

            await base.OnDisconnectedAsync(exception);
        }
    }
}
