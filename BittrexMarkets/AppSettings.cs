﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BittrexMarkets
{
    public class AppSettings
    {
        public string DataServiceUrl { get; set; }
        public int MaxSubscriptionsPerUser { get; set; }
    }
}
