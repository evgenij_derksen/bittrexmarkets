﻿using BittrexMarkets.DataModels;
using BittrexMarkets.Services.MarketsData;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BittrexMarkets.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UpdateController
    {
        private readonly IMarketsSubscriptionsService _marketsSubscriptionsService;

        public UpdateController(IMarketsSubscriptionsService marketsSubscriptionsService)
        {
            _marketsSubscriptionsService = marketsSubscriptionsService;
        }

        // POST api/update
        public async Task Post([FromBody] MarketsUpdatedRequest marketsUpdated)
        {
            await _marketsSubscriptionsService.UpdateMarketValues(marketsUpdated.updatedMarketValues);
        }

    }
}
