﻿using BittrexMarkets.DataModels;
using BittrexMarkets.Services.MarketsData.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BittrexMarkets.Services.MarketsData
{
    public interface IMarketsSubscriptionsService
    {
        AddSubscriptionResult AddMarketSubscription(string connectionId, string marketName);
        bool RemoveMarketSubscription(string connectionId, string marketName);
        List<string> RemoveAllSubscriptionsForUser(string connectionId);
        Task UpdateMarketValues(List<BittrexMarketValues> updatedMarketValues);
    }
}
