﻿using BittrexMarkets.DataModels;
using BittrexMarkets.Hubs;
using BittrexMarkets.Hubs.MarketsSubscriptions.MessageArgs;
using BittrexMarkets.Services.Data;
using BittrexMarkets.Services.MarketsData.Models;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BittrexMarkets.Services.MarketsData
{
    public class MarketsSubscriptionsService : IMarketsSubscriptionsService
    {
        private readonly IHubContext<MarketsSubscriptionsHub> _hubContext;

        private readonly IDataService _dataService;

        private Dictionary<string, HashSet<String>> _marketSubscriptionsByConnectionsId = new Dictionary<string, HashSet<String>>();

        private Dictionary<string, int> _marketsSubsCountByName = new Dictionary<string, int>();

        private int _maxSubsPerUser;

        public MarketsSubscriptionsService(IOptions<AppSettings> appSettings, IDataService dataService, IHubContext<MarketsSubscriptionsHub> hubContext)
        {
            _dataService = dataService;
            _maxSubsPerUser = appSettings.Value.MaxSubscriptionsPerUser;
            _hubContext = hubContext;
        }

        public AddSubscriptionResult AddMarketSubscription(string connectionId, string marketName)
        {
            AddSubscriptionResult result = new AddSubscriptionResult();

            if (_marketSubscriptionsByConnectionsId.ContainsKey(connectionId) == false)
            {
                _marketSubscriptionsByConnectionsId.Add(connectionId, new HashSet<string>());
            }

            var userSubscriptions = _marketSubscriptionsByConnectionsId[connectionId];

            if (userSubscriptions.Contains(marketName)
                || userSubscriptions.Count >= _maxSubsPerUser)
            {
                result.IsSuccessful = false;
                result.Message = $"Already subscribed to selected market or have >= {_maxSubsPerUser} subscriptions";
                return result;
            }

            if(_marketsSubsCountByName.ContainsKey(marketName) == false)
            {
                _marketsSubsCountByName.Add(marketName, 0);
                _dataService.AddTrackedMarket(marketName);
            }

            _marketsSubsCountByName[marketName]++;

            userSubscriptions.Add(marketName);
            result.IsSuccessful = true;

            return result;
        }

        public bool RemoveMarketSubscription(string connectionId, string marketName)
        {
            if (_marketSubscriptionsByConnectionsId.ContainsKey(connectionId))
            {
                var userSubscriptions = _marketSubscriptionsByConnectionsId[connectionId];
                if (userSubscriptions.Contains(marketName))
                {
                    userSubscriptions.Remove(marketName);
                    _marketsSubsCountByName[marketName]--;
                    if(_marketsSubsCountByName[marketName] == 0)
                    {
                        _dataService.DeleteTrackedMarket(marketName);
                    }
                }
            }

            return true;
        }

        public List<string> RemoveAllSubscriptionsForUser(string connectionId)
        {
            List<string> removedSubscriptions = new List<string>();

            if (_marketSubscriptionsByConnectionsId.ContainsKey(connectionId))
            {
                var userSubscriptions = _marketSubscriptionsByConnectionsId[connectionId];

                foreach (var userSub in userSubscriptions)
                {
                    RemoveMarketSubscription(connectionId, userSub);
                    removedSubscriptions.Add(userSub);
                }

                _marketSubscriptionsByConnectionsId.Remove(connectionId);
            }

            return removedSubscriptions;
        }

        public async Task UpdateMarketValues(List<BittrexMarketValues> updatedMarketValues)
        {
            foreach(var updatedMarketValue in updatedMarketValues)
            {
                var messageArgs = new MarketDataUpdatedMessageArgs()
                {
                    MarketName = updatedMarketValue.MarketName,
                    Ask = updatedMarketValue.Ask.ToString(),
                    Bid = updatedMarketValue.Bid.ToString(),
                    Last = updatedMarketValue.Last.ToString()
                };

                string json = JsonConvert.SerializeObject(messageArgs);
                await _hubContext.Clients.Group(updatedMarketValue.MarketName).SendAsync("MarketDataUpdated", json);
            }
        }
    }
}
