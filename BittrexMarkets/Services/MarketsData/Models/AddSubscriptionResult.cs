﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BittrexMarkets.Services.MarketsData.Models
{
    public class AddSubscriptionResult
    {
        public bool IsSuccessful { get; set; }
        public string Message { get; set; }
    }
}
