﻿using BittrexMarkets.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BittrexMarkets.Services.Data
{
    public interface IDataService
    {
        Task<List<BittrexMarketData>> GetMarketsData();
        Task<BittrexMarketValues> GetMarketValues(string marketName);
        Task AddTrackedMarket(string marketName);
        Task DeleteTrackedMarket(string marketName);
    }
}
