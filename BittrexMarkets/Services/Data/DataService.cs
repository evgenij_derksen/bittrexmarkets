﻿using BittrexMarkets.DataModels;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace BittrexMarkets.Services.Data
{
    public class DataService : IDataService
    {
        private readonly HttpClient _httpClient;
        private readonly IOptions<AppSettings> _appSettings;

        private string _dataServiceBaseUrl;

        public DataService(HttpClient httpClient, IOptions<AppSettings> appSettings)
        {
            _httpClient = httpClient;
            _appSettings = appSettings;

            _dataServiceBaseUrl = appSettings.Value.DataServiceUrl;
        }

        public async Task<List<BittrexMarketData>> GetMarketsData()
        {
            var uri = $"{_dataServiceBaseUrl}/api/markets";

            var responseString = await _httpClient.GetStringAsync(uri);

            var response = JsonConvert.DeserializeObject<DataServiceGetMarketsResponse>(responseString);

            return response.Markets;
        }

        public async Task<BittrexMarketValues> GetMarketValues(string marketName)
        {
            var uri = $"{_dataServiceBaseUrl}/api/markets/{marketName}";

            var responseString = await _httpClient.GetStringAsync(uri);

            var response = JsonConvert.DeserializeObject<BittrexMarketValues>(responseString);

            return response;
        }

        public async Task AddTrackedMarket(string marketName)
        {
            var uri = $"{_dataServiceBaseUrl}/api/tracker";

            var content = new StringContent($"\"{marketName}\"", System.Text.Encoding.UTF8, "application/json");

            var response = await _httpClient.PostAsync(uri, content);
        }

        public async Task DeleteTrackedMarket(string marketName)
        {
            var uri = $"{_dataServiceBaseUrl}/api/tracker/{marketName}";
            var response = await _httpClient.DeleteAsync(uri);
        }

    }
}
