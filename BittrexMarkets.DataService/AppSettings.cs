﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BittrexMarkets.DataService
{
    public class AppSettings
    {
        public int MarketsRefreshTimeSecs { get; set; }
        public string BittrexBaseUrl { get; set; }
        public string WebClientBaseUrl { get; set; }
    }
}
