﻿using BittrexMarkets.DataService.Models;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace BittrexMarkets.DataService.Services.Bittrex
{
    public class BittrexClient : IBittrexClient
    {
        private readonly IOptions<AppSettings> _appSettings;
        private readonly HttpClient _httpClient;
        private ILogger<BittrexClient> _logger;

        private string _bittrexBaseUrl;

        public BittrexClient(HttpClient httpClient, IOptions<AppSettings> appSettings, ILogger<BittrexClient> logger)
        {
            _appSettings = appSettings;
            _httpClient = httpClient;
            _logger = logger;

            _bittrexBaseUrl = appSettings.Value.BittrexBaseUrl;
        }

        public async Task<List<BittrexMarket>> GetBittrexMarkets()
        {
            var uri = $"{_bittrexBaseUrl}/getmarkets";

            var responseString = await _httpClient.GetStringAsync(uri);

            var response = JsonConvert.DeserializeObject<BittrexGetMarketsResponse>(responseString);

            return response.Result;
        }

        public async Task<BittrexMarketValues> GetTicker(string marketName)
        {
            var uri = $"{_bittrexBaseUrl}/getticker?market={marketName}";

            var responseString = await _httpClient.GetStringAsync(uri);

            var response = JsonConvert.DeserializeObject<BittrexGetTickerResponse>(responseString);

            return response.Result;
        }
    }
}
