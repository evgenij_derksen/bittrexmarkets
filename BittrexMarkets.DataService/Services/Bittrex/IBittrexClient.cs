﻿using BittrexMarkets.DataService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BittrexMarkets.DataService.Services.Bittrex
{
    public interface IBittrexClient
    {
        Task<List<BittrexMarket>> GetBittrexMarkets();
        Task<BittrexMarketValues> GetTicker(string marketName);
    }
}
