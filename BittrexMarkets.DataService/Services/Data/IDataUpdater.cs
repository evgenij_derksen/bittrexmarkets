﻿using BittrexMarkets.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BittrexMarkets.DataService.Services.Data
{
    public interface IDataUpdater
    {
        Task UpdateMarketsDataOnWebClient(List<BittrexMarketValues> updatedMarketValues);
    }
}
