﻿using BittrexMarkets.DataModels;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace BittrexMarkets.DataService.Services.Data
{
    public class DataUpdater : IDataUpdater
    {
        private readonly HttpClient _httpClient;

        private string _webClientBaseUrl;

        public DataUpdater(HttpClient httpClient, IOptions<AppSettings> appSettings)
        {
            _httpClient = httpClient;
            _webClientBaseUrl = appSettings.Value.WebClientBaseUrl;
        }

        public async Task UpdateMarketsDataOnWebClient(List<BittrexMarketValues> updatedMarketValues)
        {
            var uri = $"{_webClientBaseUrl}/api/update";

            var request = new MarketsUpdatedRequest();

            request.updatedMarketValues = updatedMarketValues;

            var content = new StringContent(JsonConvert.SerializeObject(request), System.Text.Encoding.UTF8, "application/json");

            var response = await _httpClient.PostAsync(uri, content);

        }
    }
}
