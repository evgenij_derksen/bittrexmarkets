﻿using BittrexMarkets.DataModels;
using BittrexMarkets.DataService.Services.Bittrex;
using BittrexMarkets.DataService.Services.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BittrexMarkets.DataService.Services.Markets
{
    public class MarketsService : IMarketsService
    {
        private readonly IBittrexClient _bittrexClient;
        private readonly IDataUpdater _dataUpdater;

        private List<BittrexMarketData> _marketsList;

        //private Dictionary<string, BittrexMarketValues> _marketsValuesByMarketsName = new Dictionary<string, BittrexMarketValues>();

        private HashSet<string> _trackedMarkets = new HashSet<string>();

        public MarketsService(IBittrexClient bittrexClient, IDataUpdater dataUpdater)
        {
            _bittrexClient = bittrexClient;
            _dataUpdater = dataUpdater;
        }

        public async Task<List<BittrexMarketData>> GetMarketsList()
        {
            if(_marketsList == null)
            {
                await LoadMarketsList();
            }

            return _marketsList;
        }

        private async Task LoadMarketsList()
        {
            var markets = await _bittrexClient.GetBittrexMarkets();

            _marketsList = new List<BittrexMarketData>();

            foreach(var market in markets)
            {
                var bittrexMarketData = new BittrexMarketData()
                {
                    BaseCurrencyName = market.BaseCurrencyLong,
                    MarketCurrencyName = market.MarketCurrencyLong,
                    MarketName = market.MarketName
                };

                _marketsList.Add(bittrexMarketData);
            }
        }

        public async Task<BittrexMarketValues> GetMarketValues(string marketName)
        {
            var values = await _bittrexClient.GetTicker(marketName);

            BittrexMarketValues bittrexMarketValues = new BittrexMarketValues()
            {
                MarketName = marketName,
                Ask = values.Ask,
                Bid = values.Bid,
                Last = values.Last
            };

            return bittrexMarketValues;
        }

        public void AddTrackedMarket(string marketName)
        {
            if(_trackedMarkets.Contains(marketName) == false)
            {
                _trackedMarkets.Add(marketName);
            }
        }

        public void DeleteTrackedMarket(string marketName)
        {
            if (_trackedMarkets.Contains(marketName))
            {
                _trackedMarkets.Remove(marketName);
            }

        }

        public async Task UpdateTrackedMarkets()
        {
            List<BittrexMarketValues> updatedMarketsValues = new List<BittrexMarketValues>();

            foreach(var trackedMarket in _trackedMarkets)
            {
                updatedMarketsValues.Add(await GetMarketValues(trackedMarket));
            }

            if(updatedMarketsValues.Count > 0)
            {
                await _dataUpdater.UpdateMarketsDataOnWebClient(updatedMarketsValues);
            }
        }

    }
}
