﻿using BittrexMarkets.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BittrexMarkets.DataService.Services.Markets
{
    public interface IMarketsService
    {
        Task<List<BittrexMarketData>> GetMarketsList();
        Task<BittrexMarketValues> GetMarketValues(string marketName);
        Task UpdateTrackedMarkets();
        void AddTrackedMarket(string marketName);
        void DeleteTrackedMarket(string marketName);
    }
}
