﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BittrexMarkets.DataService.Services.Markets
{
    internal class MarketsUpdateService : IHostedService, IDisposable
    {
        private readonly IMarketsService _marketsService;

        private int _updateDelaySeconds;
        private Timer _timer;

        public MarketsUpdateService(IMarketsService marketsService, IOptions<AppSettings> appSettings)
        {
            _marketsService = marketsService;
            _updateDelaySeconds = appSettings.Value.MarketsRefreshTimeSecs;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _timer = new Timer(DoWork, null, TimeSpan.Zero,
                TimeSpan.FromSeconds(_updateDelaySeconds));

            return Task.CompletedTask;
        }

        private void DoWork(object state)
        {
            _marketsService.UpdateTrackedMarkets();
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _timer?.Dispose();
        }
    }
}
