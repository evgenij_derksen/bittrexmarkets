﻿using BittrexMarkets.DataService.Services.Markets;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BittrexMarkets.DataService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TrackerController
    {
        private readonly IMarketsService _marketsService;

        public TrackerController(IMarketsService marketsService)
        {
            _marketsService = marketsService;
        }

        // POST api/tracker
        public void Post([FromBody] string marketName)
        {
            _marketsService.AddTrackedMarket(marketName);
        }

        // DELETE api/tracker
        [HttpDelete("{marketName}")]
        public void Delete(string marketName)
        {
            _marketsService.DeleteTrackedMarket(marketName);
        }
    }
}
