﻿using BittrexMarkets.DataModels;
using BittrexMarkets.DataService.Services.Markets;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BittrexMarkets.DataService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MarketsController : ControllerBase
    {
        private readonly IMarketsService _marketsService;

        public MarketsController(IMarketsService marketsService)
        {
            _marketsService = marketsService;
        }

        // GET api/markets
        [HttpGet]
        public async Task<string> Get()
        {
            var markets = await _marketsService.GetMarketsList();

            DataServiceGetMarketsResponse response = new DataServiceGetMarketsResponse();
            response.Markets = markets;

            return JsonConvert.SerializeObject(response);
        }

        // GET api/markets/BTC-LTC
        [HttpGet("{marketName}")]
        public async Task<string> Get(string marketName)
        {
            var marketValues = await _marketsService.GetMarketValues(marketName);

            return JsonConvert.SerializeObject(marketValues);
        }

    }
}
