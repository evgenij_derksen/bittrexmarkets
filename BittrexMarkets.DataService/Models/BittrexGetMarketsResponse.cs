﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BittrexMarkets.DataService.Models
{
    public class BittrexGetMarketsResponse : BittrexBaseResponse
    {
        [JsonProperty("result")]
        public List<BittrexMarket> Result { get; set; } = new List<BittrexMarket>();
    }
}
