﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BittrexMarkets.DataService.Models
{
    public class BittrexGetTickerResponse
    {
        [JsonProperty("result")]
        public BittrexMarketValues Result { get; set; }
    }
}
