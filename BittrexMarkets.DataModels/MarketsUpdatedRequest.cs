﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BittrexMarkets.DataModels
{
    public class MarketsUpdatedRequest
    {
        public List<BittrexMarketValues> updatedMarketValues = new List<BittrexMarketValues>();
    }
}
