﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BittrexMarkets.DataModels
{
    public class DataServiceGetMarketsResponse
    {
        public List<BittrexMarketData> Markets { get; set; } = new List<BittrexMarketData>();
    }
}
