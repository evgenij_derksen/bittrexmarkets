﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BittrexMarkets.DataModels
{
    public class BittrexMarketData
    {
        public string MarketCurrencyName { get; set; }
        public string BaseCurrencyName { get; set; }
        public string MarketName { get; set; }
    }
}
